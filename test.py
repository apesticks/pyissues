import unittest, os, pickle, sys, time
#from pyissues import RoundUp 
from pyissues.backends import FileSystemBackend
from pyissues.http import CachedRequest
from pyissues.data import PyPyRoundup

class TestFileBackend(unittest.TestCase):
    def setUp(self):
        self.tempfile = ".test_storage"

    def tearDown(self):
        if os.path.isfile(self.tempfile):
            os.remove(self.tempfile)

    def test_create(self):
        ''' Ensure that a filesystem storage is created, and that its
        saved value is what was passed in'''

        store = FileSystemBackend(self.tempfile, clear_old_data=True) 

        expected_key, expected_value = "test_key_blah", "test_data_1"
        store.save(expected_key, expected_value)

        is_file = os.path.isfile(store.filename) 
        assert is_file == True
        if is_file == True:
            with open(store.filename, "rb") as f:
                line = f.readline()
                test_key, test_value = line.split("**SEP**")

                test_key = pickle.loads(test_key.strip().replace("**NL**", "\n"))
                test_value = pickle.loads(test_value.strip().replace("**NL**", "\n"))

                assert test_key == expected_key
                assert test_value == expected_value

    def test_read(self):
        ''' Ensure that our read method returns the correct value from
        the file storage'''

        store = FileSystemBackend(self.tempfile, clear_old_data=True) 

        expected_key, expected_value = "test_key_blah", "test_data_1"
        store.save(expected_key, expected_value)

        test_value = store.read(expected_key)
        assert test_value == expected_value

    def test_update(self):
        ''' Ensure that the correct value is returned if written to more
        than once'''
        store = FileSystemBackend(self.tempfile, clear_old_data=True) 

        expected_key = "test_key_blah"
        expected_value = "test_data_2"

        store.save(expected_key, "test_data_1")
        store.save(expected_key, expected_value)

        test_value = store.read("test_key_blah")
        assert test_value == expected_value

    def test_delete(self):
        ''' Ensure that data is cleared out from the data storage '''

        store = FileSystemBackend(self.tempfile, clear_old_data=True) 

        expected_key = "test_key_blah"
        expected_value = "test_data_1"
        store.save(expected_key, expected_value)


        store.delete(expected_key)

        is_file = os.path.isfile(store.filename) 
        assert is_file == True
        if is_file == True:
            with open(store.filename, "rb") as f:
                for line in f.readlines():
                    test_key, test_value = line.split("**SEP**")

                    test_key = pickle.loads(test_key.strip())

                    assert expected_key != test_key
                    #assert expected_value != test_value

    def test_close_then_open(self):
        store = FileSystemBackend(self.tempfile, clear_old_data=True) 

        item_1, item_2, item_3 = [('key1', 'thingsup'), ('key2', 'thingz'), ('key3', 'somethingelse')]
        store.save(*item_1)
        store.save(*item_2)

        del store
        store = FileSystemBackend(self.tempfile) 
        store.save(*item_3)

        assert store.read(item_1[0]) == item_1[1]
        assert store.read(item_2[0]) == item_2[1]
        assert store.read(item_3[0]) == item_3[1]

    def test_access_stored_sequences(self):
        store = FileSystemBackend(self.tempfile, clear_old_data=True) 

        stored_list = [1,2,"other"]
        stored_dict = {"oha": "this rocks", "neat": 7}

        store.save("a_list", stored_list)
        store.save("a_dict", stored_dict)
        del store

        store = FileSystemBackend(self.tempfile)
        for index, item in enumerate(stored_list):
            assert store.read("a_list")[index] == item

        for key in stored_dict:
            assert store.read("a_dict")[key] == stored_dict[key]

#class TestHTTP(object):
class TestHTTP(unittest.TestCase):
    def setUp(self):
        self.tempfile = ".testcache"

    def tearDown(self):
        if os.path.isfile(self.tempfile):
            os.remove(self.tempfile)

    def test_fetch_cached_resource(self):
        request = CachedRequest(url="http://verygd.com", lifetime={'seconds': 2}, backendname=self.tempfile, clear_old_data=True)
        first_resp = request.response
        assert request.usecache == False
        del request

        request = CachedRequest(url="http://verygd.com", lifetime={'seconds': 2}, backendname=self.tempfile)
        second_resp = request.response

        assert request.usecache == True
        assert first_resp == second_resp
        del request

        time.sleep(3)
        request = CachedRequest(url="http://verygd.com", lifetime={'seconds': 2}, backendname=self.tempfile)
        assert request.usecache == False

    def test_save_resource(self):
        pass


unittest.main()
