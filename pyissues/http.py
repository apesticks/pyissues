import pdb
from datetime import timedelta, datetime
from urlparse import urlparse
from backends import FileSystemBackend
import requests

class Request(object):
    def __init__(self, url, *args, **kwargs):
        self.url = url
        self.user_agent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)"
        self.parse_url(url)
        self.get()

    def get(self):
        print("Fetching new URL ({})".format(self.url))
        self.response = requests.get(self.url, headers={'User-Agent': self.user_agent}, verify=False).text

    def parse_url(self, url):
        self.urldata = urlparse(url)
        
class CachedRequest(Request):
    def __init__(self, *args, **kwargs):
        self.usecache = False
        clear_old_data = kwargs.pop("clear_old_data", False)
        store_filename = kwargs.pop("backendname", ".httpcache")
        self.timedelta = timedelta(**kwargs.pop("lifetime", {"days": 1}))

        self.backend = FileSystemBackend(store_filename, clear_old_data=clear_old_data)
        super(CachedRequest, self).__init__(*args, **kwargs)

    def invalidate(self):
        super(CachedRequest, self).get()
        self.fetch_time = datetime.now()

        params = {
            "time_created": self.fetch_time, 
            "data": self.response,
        }

        self.backend.save(self.url, params)

    def get(self):
        if self.url in self.backend.keys():
            data = self.backend.read(self.url)

            if data['time_created'] + self.timedelta > datetime.now():
                entry = self.backend.read(self.url)
                self.usecache = True
                self.response = entry['data']
                self.fetch_time = entry['time_created']
            else:
                self.invalidate()
        else:
            self.invalidate()
