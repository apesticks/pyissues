from flask import Flask, render_template
from parsing import CachedRoundupParse
from models import Issue, Additional
import pdb
import traceback

app = Flask(__name__)
app.config['SQLALCHEMY_BINDS'] = {
    'issues': 'sqlite:///issues.db',
    'additional': 'sqlite:///additional.db',
}

roundup = CachedRoundupParse(app=app, clear_old_data=True)
issues = None

@app.route('/')
def index():
    return render_template("index.html") 

@app.route('/listissues')
def list_issues():
    if issues:
        return issues
    output_issues = Issue.query.all()
    return render_template("listissues.html", issues=output_issues) 

# Why does the app sometimes loop infinitely without use_reloader=False?
use_reloader = True
app.run(debug=True, use_reloader=use_reloader)
