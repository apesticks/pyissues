
current = -1;
loading = 0;

function next_loading_stage()
{
    if (loading != -1) {
        loading++;
        loading %= 7;
        var s = "Loading ";
        for (var i = 0; i < loading; ++i) {
            s += ".";
        }
        $("#loading").html(s);
        setTimeout(next_loading_stage, 500);
    }
}

function filter(elem, search_val)
{
    var elem = $(elem);
    if (elem.text().indexOf(search_val) != -1) {
        elem.show();
    } else {
        elem.hide();
    }
}

function openissue(num)
{
    if (current != -1 && num != current) {
        $("#issue" + current).toggle();
    }
    $("#issue" + num).toggle();
    if (num != current) {
        var pos = $("#issue" + num).position().top - 50;
        $.scrollTo(pos, 200, {axis: 'y'});
        current = num;
        //window.location.href = window.location.origin + "/#" + num;
    } else {
        current = -1;
    }
}

function post(num)
{
    var new_comment = $("#txt" + num).val();
    var new_author = $("#author" + num).val();
    $.post("/post_comment",
           {'num': num, 'comment': new_comment, 'author': new_author},
           function (res) {
               $("#issuebody" + num).html(res);
               $("#txt" + num).val("");
           });
}

$(document).ready(function () {
    next_loading_stage();
    $.get("listissues", function (data) {
        var loc = window.location.href;
        var num = parseInt((loc.substr(loc.lastIndexOf('/') + 2)));
        loading = -1;
        $("#list").html(data);
        if (num) {
            openissue(num);
        }
        $("#searchbox").focus();
    });
    $("#searchbox").keydown(function () {
        setTimeout(function() {
            var search_val = $("#searchbox").val()
            $(".issuepreview").each(function (index, elem) {
                filter(elem, search_val);
            });
        }, 0);
    });
})
