from flask.ext.sqlalchemy import SQLAlchemy
import pdb

# mental note for later: make a tool to coerce a list of random python datatypes to a Flask-SQLAlchemy model
db = SQLAlchemy()

class Additional(db.Model):
    #__tablename__ = 'additional'
    __bind_key__ = 'additional'
    id = db.Column(db.Integer(80), primary_key=True, autoincrement=True)
    fetch_time = db.Column(db.DateTime)

    def __init__(self, fetch_time):
        self.fetch_time = fetch_time

class Issue(db.Model):
    __bind_key__ = 'issues'

    id = db.Column(db.Integer(80), primary_key=True)
    title = db.Column(db.String(80), unique=False)
    status = db.Column(db.String(80), unique=False)
    priority = db.Column(db.String(80), unique=False)
    creation = db.Column(db.String(80), unique=False)
    activity = db.Column(db.String(80), unique=False)
    creator = db.Column(db.String(80), unique=False)
    actor = db.Column(db.String(80), unique=False)
    assignedto = db.Column(db.String(80), unique=False)
    nosy = db.Column(db.String(80), unique=False)
    superseder = db.Column(db.String(80), unique=False)
    messages = db.Column(db.String(80), unique=False)
    files = db.Column(db.String(80), unique=False)

    def __init__(self, id, title, status, priority, creation, activity, creator, actor, assignedto, nosy, superseder, messages, files):
        # doing it the stupid way for now
        self.id = id
        self.title = title
        self.status = status
        self.priority = priority
        self.creation = creation
        self.activity = activity
        self.creator = creator
        self.actor = actor
        self.assignedto = assignedto
        self.nosy = nosy
        self.superseder = superseder
        self.messages = messages
        self.files = files

    def __repr__(self):
        return "<Issue {}: {}>".format(self.id, self.title)


