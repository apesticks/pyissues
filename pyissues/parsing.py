from models import db, Issue, Additional

from http import CachedRequest
from unidecode import unidecode
from collections import OrderedDict
from datetime import timedelta, datetime
import csv, io, codecs, os
import pdb

class RoundupParse(object):
    def __init__(self, *args, **kwargs):
        self.lifetime = {"days": 2}
        self.csvdata = self.fetch_latest_issues()

    def fix_row(self, row):
        try:
            row[0] = int(row[0])
        except IndexError:
            pass
        return row

    def csv_to_list(self, data):
        f = io.StringIO()
        full = []

        data = unicode(unidecode(data))
        f.write(data)
        f.seek(0)

        raw_data = csv.reader(f)
        for rownum, row in enumerate(raw_data):
            if rownum == 0:
                self.headers = row
                continue

            row = self.fix_row(row)

            yield OrderedDict(zip(self.headers, row))
        #    full.append(OrderedDict(zip(self.headers, row)))
        #return full

    def fetch_latest_issues(self):
        #id,title,status,priority,creation,activity,creator,actor,assignedto,nosy,superseder,messages,files,keyword

        export_url = 'http://bugs.pypy.org/issue?@action=export_csv&@columns=id,title,status,priority,creation,activity,creator,actor,assignedto,nosy,superseder,messages,files&@sort=creation&@pagesize=1500&@startwith=0'
        data = CachedRequest(export_url, lifetime=self.lifetime, backendname=".pypybugs")
        self.fetch_time = data.fetch_time
        return self.csv_to_list(data.response)

class CachedRoundupParse(RoundupParse):
    """ TODO: Genericize this class to handle more than just Roundup """
    def __init__(self, *args, **kwargs):
        super(CachedRoundupParse, self).__init__(*args, **kwargs)

        self.usecache = False
        clear_old_data = kwargs.pop("clear_old_data", False)
        store_filename = kwargs.pop("backendname", "issues.db")
        self.timedelta = timedelta(**self.lifetime)

        self.app = kwargs.pop('app')
        import models
        db.init_app(self.app)
        self.backend = db
        #self.backend = kwargs.pop("database")
        #self.model = kwargs.pop('model')
        #self.model_additional = kwargs.pop('model_additional')
        if clear_old_data == True:
            self.fetch_time = (self.fetch_time - timedelta(days=3))

        self.get()

    def latest_fetch_time(self):
        try:
            return max([row.fetch_time for row in Additional.query.all()])
        except Exception as e:
            return self.fetch_time - timedelta(days=3)

    def get(self):
        with self.app.test_request_context():
            if self.latest_fetch_time() + self.timedelta > datetime.now():
                self.usecache = True
            else:
                self.invalidate()

    def bootstrap_db(self):
        """ Clears the issue table if it exists, creates a new one.
        Additionally, creates the 'additional' database if it does not
        exist
        
        Note: A slightly ugly hack is used to gain access to flask's 
        request context for db use """

        db_additional = self.backend.get_engine(self.backend.get_app(), 'additional')

        if os.path.getsize(db_additional.engine.url.database) == 0:
            self.backend.create_all(bind='additional')

        self.backend.drop_all(bind='issues')
        self.backend.create_all(bind='issues')

    def populate_db(self):
        for index, item in enumerate(self.to_parse):
            try:
                if len(item) > 0:
                    entry = Issue(*item.values())
                    self.backend.session.add(entry)
            except Exception as e:
                pdb.set_trace()

        self.fetch_time = Additional(datetime.now()) 
        self.backend.session.add(self.fetch_time)

    def invalidate(self):
        print "Invalidating parse cache"
        self.to_parse = self.fetch_latest_issues()

        # Clear the issue database and start anew
        self.bootstrap_db()
        self.populate_db()
        self.backend.session.commit()

    def __getitem__(self, key):
        for item in self.data:
            if item['id'] == key:
                return item.values()
        raise Exception("No issue with id {} exists".format(key))

