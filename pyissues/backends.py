import os
import pdb
import pickle

class Backend(object):
    def __init__(*args, **kwargs):
        pass

    def save(key, data):
        raise NotImplementedError

    def read(key):
        raise NotImplementedError


class DatabaseBackend(Backend):
    pass


class SqliteBackend(DatabaseBackend):
    pass


class FileSystemBackend(Backend):
    def __init__(self, filename=".cached_storage", clear_old_data=False):
        self.filename = filename
        self.data = {}
        self.imported = False
        self.dummy_pickle_newline = "**NL**"
        self.dummy_kv_divider = "**SEP**"

        if clear_old_data == True:
            if os.path.isfile(filename):
                os.remove(filename)
                
        if os.path.isfile(self.filename):
            self.import_kv_pairs()
        else:
            # Create a dummy file
            with open(self.filename, 'a') as f:
                os.utime(self.filename, None)

    def open_file(self, location, mode="rb"):
        return open(self.filename, mode)

    def pickle_item(self, data):
        if len(data) > 2:
            raise("Something went wrong!")
        return [pickle.dumps(value).replace("\n", self.dummy_pickle_newline) for value in data]

    def unpickle_item(self, data):
        if len(data) > 2:
            raise("Something went wrong!")
        return [pickle.loads(value.replace(self.dummy_pickle_newline, "\n")) for value in data]

    def import_kv_pairs(self):
        """ untested """
        with self.open_file(self.filename, "rb") as f:
            for line in f.readlines():
                item = [thing.strip("\n") for thing in line.split(self.dummy_kv_divider)]
                item = self.unpickle_item(item)
                new_kv = dict([item])
                self.data.update(new_kv)
            self.imported = True

    def write_kv_pairs(self):
        with self.open_file(self.filename, "wb") as f:
            for item in self.data.items():
                item = self.pickle_item(item)
                #f.write('{}:{}\n'.format(*item))
                f.write(self.dummy_kv_divider.join(item) + "\n")

    def save(self, key, value):
        self.data[key] = value
        self.write_kv_pairs()

    def read(self, key):
        return self.data[key]

    def delete(self, key):
        del self.data[key]
        self.write_kv_pairs()

    def keys(self):
        return self.data.keys()
